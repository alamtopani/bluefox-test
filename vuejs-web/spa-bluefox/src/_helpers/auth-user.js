export function authUser() {
  let apiUrl = 'https://www.juventiniindonesia.com'
  let user = JSON.parse(localStorage.getItem('user'));
  let authorize = {}

  if (user && user.token) {
    authorize = { 'Authorization': 'Bearer ' + user.token };
  }

  const requestOptions = {
    method: 'GET',
    headers: authorize
  };

  return fetch(apiUrl+'/api/v1/check-token', requestOptions)
    .then(handleResponse)
}

function handleResponse(response) {
  if (!response.ok) {
    localStorage.removeItem('user');
    window.location.replace("/login");
  }
}