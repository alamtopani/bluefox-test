import Vue from 'vue'
import App from './App'
import router from './router'
import Axios from 'axios'
import StarRating from 'vue-star-rating'

Vue.config.productionTip = false
Vue.prototype.$http = Axios;
Vue.component('star-rating', StarRating);
Vue.prototype.$apiUrl = 'https://www.juventiniindonesia.com'

new Vue({
  router,
  components: { App, StarRating },
  render: h => h(App)
}).$mount('#app')
