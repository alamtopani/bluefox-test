import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import Dashboard from '@/components/dashboard/Dashboard'
import User from '@/components/dashboard/User'
import ListFiles from '@/components/dashboard/ListFiles'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'HelloWorld', component: HelloWorld },
    { path: '/login', name: 'Login', component: Login},
    { path: '/register', name: 'Register', component: Register},
    { path: '/dashboard', name: 'Dashboard', component: Dashboard },
    { path: '/user/:id', name: 'User', component: User},
    { path: '/files', name: 'ListFiles', component: ListFiles}
  ]
})
