class Review < ApplicationRecord
  default_scope {order(created_at: :desc)}
  
	belongs_to :user
	belongs_to :rater, class_name: 'User', foreign_key: 'rater_id'
end
