class UploadFile < ApplicationRecord
  default_scope {order(created_at: :desc)}
  belongs_to :user

  mount_uploader :link_file, AttachmentUploader

  def file_url_link?
    return "#{Rails.application.secrets.host}#{self.link_file.url}"
  end
  
end
