class User < ApplicationRecord
	attr_accessor :password

	has_many :tokens
	has_many :reviews, dependent: :destroy
	has_many :upload_files, dependent: :destroy

	validates_presence_of :username, :email, :full_name
	validates_uniqueness_of :username, :email

	before_save :encrypt_password

	mount_uploader :avatar, AvatarUploader

  include BCrypt
  
	def avatar_url?
		return "#{Rails.application.secrets.host}#{avatar.url}"
  end

	def activated?
		self.activated == true
	end

	def inactivated?
		self.activated == false
	end

	def has_password?(password)
    BCrypt::Password.new(crypted_password) == password
	end

	def encrypt_password
		self.crypted_password = BCrypt::Password.create(password) if password.present?
	end

	def generate_token
		tokens.destroy_all
    expiry_date = Time.now + 1.weeks
    payload = { identifier: self.id, expiry_date: expiry_date }
    token = JWT.encode(payload, 'SIGNATURE-KEY-BASE', 'HS256')
    tokens.create( kind: 'jwt', token: token, expiry_date: expiry_date)
	end

	def delete_token
		tokens.destroy_all
	end

	def total_rate?
		(reviews.sum(:rating) / reviews.size) rescue 0
	end
end
