module Versions
  module Kernel
    extend ActiveSupport::Concern

    included do
      version 'v1', using: :path
      format :json
      prefix :api
      default_format :json

      helpers do
        def auth_token
          headers['Authorization'].split('Bearer ')[1] if headers['Authorization'].present?
        end

        def current_user
          error!('Token not found', 400) unless auth_token

          decoded_data = JWT.decode(auth_token, 'SIGNATURE-KEY-BASE', 'HS256')
          error!('Token has been expired', 400) if Time.now.to_i > decoded_data.first['expiry_date'].to_datetime.to_i

          token = Token.find_by_token auth_token
          error!('Token not found', 400) unless token

          user = User.find decoded_data.first['identifier']
        end

        def authenticate
          error!('401 Unauthorized', 401) unless current_user
        end

        def permitted_params
          @permitted_params ||= declared(params, include_missing: false)
        end

        def logger
          Rails.logger
        end
      end

      rescue_from ActiveRecord::RecordNotFound do |e|
        error_response(message: e.message, status: 404)
      end

      rescue_from ActiveRecord::RecordInvalid do |e|
        error_response(message: e.message, status: 422)
      end
    end
  end
end