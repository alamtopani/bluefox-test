module Versions::V1::Endpoints
  class UserEndpoint < Grape::API
    helpers do 
      def check_authorization
        case current_user.role
        when 'member'
          error!('You do not have access this action!', 400)
        end
      end
    end

    desc '------------ Check Token ---------------'
    get '/check-token' do
      @data = current_user

      if @data.present?
        present @data, with: Versions::V1::Entities::UserEntity
      else
        error!('Access denied')
      end
    end

    desc '------------ Logout User ---------------'
    delete '/logout' do 
      current_user.delete_token
    end

    desc "------------ login user ---------------"
    params do
      requires :identifier, type: String, allow_blank: false
      requires :password, type: String, allow_blank: false
    end

    post '/login' do
      @user = User.where("users.email =? or users.username =?", params[:identifier], params[:identifier]).last

      error!('Email or Username Not Found', 401) if @user.blank?
      error!('Your account is inactive', 401) if @user.inactivated?
      error!('Wrong Password') unless @user.has_password?(params[:password])

      @user.generate_token
      present @user, with: Versions::V1::Entities::UserEntity
    end

    desc "------------ register user ---------------"
    params do
      requires :role, type: String, allow_blank: false, values: ['member', 'admin']
      requires :email, type: String, allow_blank: false
      requires :username, type: String, allow_blank: false
      requires :full_name, type: String, allow_blank: false
      requires :password, type: String, allow_blank: false
    end

    post '/register' do
      @user = User.new(params)
      if @user.save
        present @user, with: Versions::V1::Entities::UserEntity
      else
        error!(@user.errors.full_messages, 400)
      end
    end

    desc "------------ list users ---------------"
    get '/users' do
      current_user
      @data = Member.all
      present @data, with: Versions::V1::Entities::UserEntity
    end

    desc "------------ show user ---------------"
    get '/user/:id' do
      current_user
      @data = User.find params[:id]
      present @data, with: Versions::V1::Entities::UserEntity
    end

    desc "------------ deleted user ---------------"
    delete '/user/:id' do
      check_authorization
      @data = User.find params[:id]
      @data.destroy
      present @data, with: Versions::V1::Entities::UserEntity
    end

    desc "------------ review rating user ---------------"
    params do
      optional :comment, type: String
      requires :user_id, type: Integer, allow_blank: false
      requires :rating, type: Integer, allow_blank: false
    end

    post '/user-rating' do 
      @review = Review.new(params)
      @review.rater_id = current_user.id
      if @review.save
        present @review.user, with: Versions::V1::Entities::UserEntity
      else
        error!(@review.errors.full_messages, 400)
      end
    end

    desc "------------ list upload files ---------------"
    get '/upload-files' do
      current_user
      @data = UploadFile.all
      present @data, with: Versions::V1::Entities::UploadFileEntity
    end

    desc "------------ input upload files ---------------"
    params do
      optional :name, type: String
      requires :link_file, type: File
    end

    post '/upload-files' do
      @upload = UploadFile.new(params)
      @upload.user_id = current_user.id
      if @upload.save
        present @upload, with: Versions::V1::Entities::UploadFileEntity
      else
        error!(@upload.errors.full_messages, 400)
      end
    end

    desc "------------ deleted files ---------------"
    delete '/upload-files/:id' do
      check_authorization

      @data = UploadFile.find params[:id]
      @data.destroy
      present @data, with: Versions::V1::Entities::UploadFileEntity
    end

  end
end


