module Versions::V1::Entities
  class ReviewEntity < Grape::Entity
		expose :id
		expose :comment
		expose :rating
		expose :created_at
		expose(:rater) {|r| r.rater.full_name rescue nil}
	end
end