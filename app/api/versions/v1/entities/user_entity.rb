module Versions::V1::Entities
  class UserEntity < Grape::Entity
		expose :id, :role, :full_name, :username, :email
		expose :activated
    expose :created_at
		expose(:avatar) {|r| r.avatar_url?}
		expose(:token) {|r| r.tokens.last.token rescue nil}
		expose(:token_expired) {|r| r.tokens.last.expiry_date rescue nil}
		expose(:total_rating) {|r| r.total_rate?}
		expose :reviews, using: Versions::V1::Entities::ReviewEntity
	end
end