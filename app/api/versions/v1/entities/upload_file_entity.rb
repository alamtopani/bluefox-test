module Versions::V1::Entities
  class UploadFileEntity < Grape::Entity
    expose :id
    expose :name
    expose(:link) {|u| u.file_url_link?}
    expose :created_at
    expose(:user_name) {|u| u.user.full_name}
	end
end