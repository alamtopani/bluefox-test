module Versions
	class Base < Grape::API
		include Versions::Kernel
		
		mount Versions::V1::Endpoints::UserEndpoint
	end
end
