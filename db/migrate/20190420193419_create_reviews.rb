class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.integer :rating, default: 0
      t.text :comment
      t.integer :user_id
      t.integer :rater_id

      t.timestamps
    end

    add_index :reviews, :rater_id
    add_index :reviews, :user_id
  end
end
