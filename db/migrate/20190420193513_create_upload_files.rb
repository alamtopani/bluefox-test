class CreateUploadFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :upload_files do |t|
      t.string :name
      t.string :link_file
      t.integer :user_id

      t.timestamps
    end
  end
end
