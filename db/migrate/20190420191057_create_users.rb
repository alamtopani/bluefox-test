class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :role
      t.string :full_name
      t.string :username
      t.string :email
      t.string :crypted_password
      t.integer :role_id
      t.string :avatar
      t.boolean :activated, default: true

      t.timestamps
    end

    add_index :users, :role_id
  end
end
