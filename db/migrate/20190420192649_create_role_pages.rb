class CreateRolePages < ActiveRecord::Migration[5.2]
  def change
    create_table :role_pages do |t|
      t.string :action_name
      t.boolean :action_status
      t.integer :role_id

      t.timestamps
    end
  end
end
